import { createRouter, createWebHistory } from 'vue-router'
import displayAll from '../components/displayAll.vue'
import newTopic from '../components/newTopic.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: displayAll
  },
  {
    path: '/new_topic',
    name: 'New topic',
    component: newTopic

  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
