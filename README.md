# front

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
### :warning: Bypass CORS errors

- Install "CORS Everywhere" extension for Mozilla Firefox

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
